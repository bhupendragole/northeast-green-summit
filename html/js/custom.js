//Header Jquery
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 240) {
        $(".fixed-top").addClass("lightheader");
    } else {
        $(".fixed-top").removeClass("lightheader");
    }
});


//Page scroll jquery

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 30)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

})(jQuery); // End of use strict


//Partner by Jquery
$(document).ready(function() {
  $('#lightslider-demo').lightSlider({
    controls: true,
    item: 7,
    loop: false,
    pager: false,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          item: 4
        }
      }
    ]
  });
});

//Menu Jquery		

$(".navbar-toggler").click(function(){
    $("#navbarResponsive").slideToggle();
});


$(document).ready(function() {
    function checkWidth() {
        var windowSize = $(window).width();

        if (windowSize < 767) {
            $(".navbar-nav .nav-link").click(function(){
				$("#navbarResponsive").slideToggle();
			});

        }
    }

    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
});

//Gallery Jquery
	$('#galleryCarousel').carousel({
			interval: 5000
	});
	
//Light Box Jquery	
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });